import 'package:flutter/material.dart' show Center, Text, TextDirection, runApp;

void main() {
  runApp(Center(
    child: Text(
      "Hello , world",
      textDirection: TextDirection.ltr,
    ),
  ));
}
